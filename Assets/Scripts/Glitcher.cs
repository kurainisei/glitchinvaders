﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kino;
using UnityEngine.SceneManagement;

public class Glitcher : MonoBehaviour {
    public AnalogGlitch glitchengine;
    public float glitchRate;
	// Use this for initialization
	void Start () {
        StartCoroutine(Glitch());

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResetGlitch()
    {
        glitchengine.scanLineJitter = 0;
    }

    IEnumerator Glitch()
    {
        while (glitchengine.scanLineJitter<=1)
        {
            yield return new WaitForSeconds(glitchRate);
            glitchengine.scanLineJitter += 0.1f;
        }

        //call gameover
        SceneManager.LoadScene("GameOver");
        yield return null;
    }
}

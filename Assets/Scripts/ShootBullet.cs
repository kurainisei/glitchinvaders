﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBullet : MonoBehaviour {
    public GameObject bulletPrefab;
    public Transform shootingPlace;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            Instantiate(bulletPrefab, shootingPlace.position,Quaternion.identity);
        }
	}
}

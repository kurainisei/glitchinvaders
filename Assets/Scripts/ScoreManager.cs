﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    public int score;
    public Text scoreT;
	// Use this for initialization
	void Start () {
        score=0;
        scoreT.text = score.ToString();
    }
	
	public void IncrementScore()
    {
        score += 100;
        scoreT.text = score.ToString();
    }

    public void SaveScore()
    {
        //this function will save the score on a JSON file stored on server upon GameOver.
        //it will also increment the overall number of plays on the same JSON file.
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public GameObject enemy;
    public float spawnRate;
	// Use this for initialization
	void Start () {
		StartCoroutine(SpawnEnemies());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnRate);
            {
                for (int i = 0; i <= 5; i++)
                {
                    Instantiate(enemy,new Vector2(transform.position.x + 1.5f * i, transform.position.y),Quaternion.identity); 
                }
            }
        }
    }
}

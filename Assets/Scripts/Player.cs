﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int Lives = 3;
    public float Speed;

    Rigidbody2D rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        float _newXPosition = Mathf.Clamp(rb.position.x + Speed * Input.GetAxis("Horizontal") * Time.deltaTime, -5, 5) ;
        rb.MovePosition(new Vector2(_newXPosition, rb.position.y));
	}

    public void Kill()
    {
        //Do kill stuff
        Lives--;
        if (Lives == 0)
        {
            //Call GameOver
        }
    }
}

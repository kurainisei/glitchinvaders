﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //this could be done better by using ObjectPooling. Leave like that for now.
        Destroy(collision.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {
    public float MovementInterval;
    public float MovementHorizontal;
    public float MovementVertical;

    Rigidbody2D _rb;
    private int _movementStep;

	// Use this for initialization
	void Start () {
        _movementStep = 0;
        _rb = GetComponent<Rigidbody2D>();
        StartCoroutine(Move());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Move()
    {
        while (true)
        {
            
            switch (_movementStep)
            {
                case 0:
                    _rb.MovePosition(transform.position + transform.right * MovementHorizontal);
                    break;
                case 1:
                    _rb.MovePosition(transform.position - transform.up * MovementVertical);
                    break;
                case 2:
                    _rb.MovePosition(transform.position - transform.right * MovementHorizontal);
                    break;
                case 3:
                    _rb.MovePosition(transform.position - transform.up * MovementVertical);
                    break;
            }
            _movementStep = (_movementStep + 1) % 4;
            //Debug.Log(_movementStep); 
            yield return new WaitForSeconds(MovementInterval);
        }
       
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //load GameOver
            SceneManager.LoadScene("GameOver");
        }
    }
}

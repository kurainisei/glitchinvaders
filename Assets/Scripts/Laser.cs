﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {
    public float Speed;
    Rigidbody2D rb;
    ScoreManager score;

    // Use this for initialization
    void Start()
    {
        score = FindObjectOfType<ScoreManager>();
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.up * Speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag=="Enemy")
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Camera.main.GetComponent<Glitcher>().ResetGlitch();
            score.IncrementScore();
        }
    }
}
